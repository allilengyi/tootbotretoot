# TootBot

A small python 3.x script to replicate tweets on a mastodon account.

The script only needs mastodon login/pass to post toots.

It gets the tweets from RSS available at nitter.net/$TWITTER/rss, then does some cleanup on the content.

A sqlite database is used to keep track of tweets than have been tooted.



The script is simply called by a cron job and can be run on any server (does not have to be on the mastodon instance server).

This is a fork of https://github.com/corbindavenport/tootbot.

## Setup

```
# clone this repo
git clone https://0xacab.org/unsuspicious/tootbotretoot.git
cd tootbot

# install required python modules
pip3 install -r requirements.txt
```

## Useage

`python3 tootbot.py <twitter_pseudo> <mastodon_account> <mastodon_password> <mastodon_domain>`

Example:

`python3 tootbot.py geonym_fr geonym@mastodon.mydomain.org **password** mastodon.mydomain.org`

Edit cron_sample.sh to taste to add this to your crontab.
