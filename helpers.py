#!/usr/bin/python3
import pycurl
from io import BytesIO
import re
import subprocess
import magic

def preceeding_tootid(nitterinstance,db,twitter,tweetid):
  buffer = BytesIO()
  curl = pycurl.Curl()
  curl.setopt(curl.URL, 'https://'+nitterinstance+'/'+twitter+'/status/'+tweetid+'#m')
  curl.setopt(curl.WRITEDATA, buffer)
  curl.perform()
  curl.close()
  
  body = buffer.getvalue()
  # Body is a byte string.
  # We have to know the encoding in order to print it to a text file
  # such as standard output.
  idnumber=""
  for line in body.decode('utf-8').splitlines():
  # search for occurence of `class="tweet-link"`
    if re.match(".*class=\"tweet-link\".*",line) is not None:
       # this will result in errors if twitter handle contains numbers
       # get twitter id of linked tweet
       idnumber=re.findall('\d+',line)[0]
  # break if `class="main-tweet"` is found
    elif re.match(".*class=\"main-tweet\".*",line) is not None:
       break
  # I've read this way is best practice
  tid = (tweetid,)
  db.execute('SELECT * FROM tweets WHERE tweet=?', tid)
  result=db.fetchone()
  if result is not None:
    print("Preceeding toot id is: "+result[1])
    return result[1]
  else:
    return None

def uploadVideo(nitterinstance,twitter,tweetid,mastodon_api,toot_media):
  buffer = BytesIO()
  curl = pycurl.Curl()
  curl.setopt(curl.URL, 'https://'+nitterinstance+'/'+twitter+'/status/'+tweetid+'#m')
  curl.setopt(curl.WRITEDATA, buffer)
  curl.perform()
  curl.close()

  body = buffer.getvalue()
  # Body is a byte string.
  # We have to know the encoding in order to print it to a text file
  # such as standard output.
  # print(body.decode('utf-8'))
  isVideo=False
  for line in body.decode('utf-8').splitlines():
    if re.match(".*\<meta property=\"og:type\" content=\"video\" /\>.*",line) is not None:
      isVideo=True
      break
  if isVideo:
    #get ID
    URL="https://twitter.com/"+twitter+"/status/"+tweetid
    subprocess.run(["youtube-dl","-o","%(id)s.%(ext)s",URL],stdout=subprocess.PIPE)
    dURL = subprocess.run(["youtube-dl","--get-filename","-o","%(id)s.%(ext)s",URL],stdout=subprocess.PIPE,stderr=subprocess.PIPE, text=True)
    filename = dURL.stdout.rstrip('\n')
    if dURL.stderr is "":
      # video is successfully downloaded, proceed
      mime = magic.Magic(mime=True)
      # get mime type
      media_posted = mastodon_api.media_post(filename, mime_type=mime.from_file(filename), description="Leider kann der Bot keine Medienbeschreibungen bieten")
      toot_media.append(media_posted['id'])
      # remove downloaded video
      os.remove(filename)
      return isVideo
    else:
      print(dURL.stderr)
      return False 
