# activate virtualenv if necessary
# source /home/cquest/.virtualenvs/tootbot/bin/activate

# parameters:
# 1- twitter account to clone
# 2- mastodon login
# 3- mastodon password
# 4- instance domain (https:// is automatically added)

python3 tootbot.py geonym_fr geonym@amicale.net **password** test.amicale.net
python3 tootbot.py cq94 cquest@amicale.net **password** test.amicale.net

# you need to add this script to your crontab so that this script is automatically called from time to time. 
# for this, enter crontab -e and add, for example, this line
# 15, 30, 45, 00 * * * * /path/to/cron-sample.sh
# this means that your mirror will do its job every 15 minutes
